# HTML5 platformer #

This is a learning project to understand game development using HTML5 canvas and JavaScript.
Playable at http://tunzor.com/games/plat

## Learning points ##
* Jump mechanics with gravity effect
* Smooth character movement when key is pressed and held (adjusting velocity by keypress/keyup instead of changing position on keypress)